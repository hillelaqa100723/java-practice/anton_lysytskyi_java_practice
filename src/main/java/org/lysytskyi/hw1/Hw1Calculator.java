package org.lysytskyi.hw1;

import java.util.Scanner;

public class Hw1Calculator {

    public static void main(String[] args) {

        Scanner input=new Scanner(System.in);

        int a;
        int b;

        System.out.print("Enter a: ");
        a = input.nextInt();

        System.out.print("Enter b: ");
        b = input.nextInt();

        printResult("Sum: ", plus(a, b));
        printResult("Odd: ", minus(a, b));
        printResult("Mult: ", mult(a, b));
        printResult("Div: ", div(a, b));
        printResult("Rem: ", rem(a, b));

    }

    public static void printResult(String comment, int value) {
        System.out.println(comment + value);
    }

    public static int plus(int p, int r) {
        return p + r;
    }

    public static int minus(int p, int r) {
        return p - r;
    }

    public static int mult(int p, int r) {
        return p * r;
    }

    public static int div(int p, int r) {
        if (r != 0) {
            return p / r;
        } else {
            System.out.println("You cannot divine by 0!");
            return -1000000;
        }
    }

    public static int rem(int p, int r) {
        if (r != 0) {
            return p % r;
        } else {
            System.out.println("You cannot divine by 0!");
            return -1000000;
        }
    }
}
